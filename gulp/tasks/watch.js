module.exports = function() {
    $.gulp.task('watch',function(){
        $.gulp.watch('app/templates/**/*.pug',$.gulp.series('pug'));
        $.gulp.watch('app/styles/**/*.scss',$.gulp.series('sass'));
        $.gulp.watch('app/js/*.js',$.gulp.series('scripts'));
        $.gulp.watch('app/img/*',$.gulp.series('img'));
        $.gulp.watch('app/fonts/*',$.gulp.series('fonts'));
        $.gulp.watch('app/svg/*',$.gulp.series('svg'));
    });
};
