'use strict';

var app = {

  init: function () {

    app.animation.init();
    app.copy.init();
    app.mfp.init();

    $(window).on('resize', function (e) {


    }).trigger('resize');

    $(window).on('load', function () {


    });

  },

  animation: {


    init: function () {

      app.animation.fade();
      app.animation.services();
      app.animation.creat();
      app.animation.header_creat();
      // app.animation.percent();

    },

    fade: function () {

      var animation = $('.animation');

      if ( !animation.length ) return;

      animation.each(function () {

        var $this = $(this);
        var $this_offset = $(this).offset().top - $(window).height() / 2;

        $(window).on('scroll', function (e) {

          if (pageYOffset > $this_offset) {

            $this.addClass('is-visible');

          }

        });

      });


    },

    percent: function () {

      var percent = $('.percent');
      var percent_value = $('.percent__range_value');

      var img_1 = $('.percent__move .img1');
      var img_2 = $('.percent__move .img2');
      var img_3 = $('.percent__move .img3');

      var percent_height, percent_offset_t, percent_offset_b, percent_diff;

      var lastScrollTop = 0;

      $(window).on('scroll', function (e) {

        percent_height = percent.outerHeight();
        percent_offset_t = percent.offset().top;

        percent_offset_b = percent_offset_t + percent_height;

        percent_diff = percent_offset_b - percent_offset_t;
        percent_offset_b = percent_offset_t + percent_height;

        percent_diff = percent_offset_b - percent_offset_t;

        console.log();

        if ( pageYOffset + $(window).height() >= percent_offset_b  ) {

          // $('body').css('overflow', 'hidden');

        }


        if (pageYOffset > percent_offset_t - 360 && pageYOffset < percent_offset_b) {

          var ww = $(window).width();

          var img_1_pos_left = img_1.position().left;
          var img_2_pos_left = img_2.position().left;
          var img_3_pos_left = img_3.position().left;

          var img_1_offset_left = img_1.offset().left;
          var img_2_offset_left = img_2.offset().left;
          var img_3_offset_left = img_3.offset().left;

          img_1.css('left', img_1_pos_left + 20);

          if (img_1_offset_left > ww / 2.25) {
            img_2.css('left', img_2_pos_left + 17);

            if (!img_1.hasClass('ready') && img_1_offset_left > ww ) {

              img_1.addClass('ready');

              var width = 10

              var id = setInterval(function () {

                  if (width >= 35) {

                    clearInterval(id);

                    img_2.addClass('start');

                  } else {
                    width++;
                    percent_value.css('width', width + '%');
                    percent_value.text(width + '%');
                  }

              }, 40, );

            }

          }

          if (img_2_offset_left > ww / 2.25) {
            img_3.css('left', img_3_pos_left + 15);

            if (!img_2.hasClass('ready') && img_2_offset_left > ww ) {

              img_2.addClass('ready');

              var width = 35

              var id = setInterval(function () {

                if ( !img_2.hasClass('start') ) return;

                if (width >= 70) {

                  clearInterval(id);

                  img_3.addClass('start');

                } else {
                  width++;
                  percent_value.css('width', width + '%');
                  percent_value.text(width + '%');
                }

              }, 40, );

            }

          }

          if (img_3_offset_left > ww / 2.25) {

            if (!img_3.hasClass('ready') && img_3_offset_left > ww ) {

              img_3.addClass('ready');

              var width = 70

              var id = setInterval(function () {

                console.log('wait');

                if ( !img_3.hasClass('start') ) return;

                console.log('go');

                if (width >= 100) {

                  clearInterval(id);

                } else {
                  width++;
                  percent_value.css('width', width + '%');
                  percent_value.text(width + '%');
                }

              }, 40, );

            }

          }



          var st = $(this).scrollTop();
          if (st > lastScrollTop) {



          } else {
            // $('body').removeClass('active');
          }
          lastScrollTop = st;

        }

      });

    },

    services: function () {

      if ( !$('.services-animation').length ) return;

      var services_animation = $('.services-animation').offset().top - $(window).height() / 2;

      $(window).on('scroll', function (e) {

        if (pageYOffset > services_animation) {

          $('.services-animation').addClass('is-visible');

        }

      });

    },

    creat: function () {

      if ( !$('.creat-animation').length ) return;

      var creat_animation = $('.creat-animation').offset().top + $('.creat-animation').outerHeight() - 50;

      $(window).on('scroll', function (e) {

        console.log( pageYOffset + $(window).height() , creat_animation );

        if (pageYOffset + $(window).height() >= creat_animation) {

          $('.creat-animation').addClass('is-move');

        }

      });

    },

    header_creat: function () {

      var creat = $('.header__creat');

      $(window).on('scroll', function () {

        var examples = $(".examples");

        if ( !examples ) return;

        var examples_h = examples.height();
        var examples_t = examples.offset().top;
        var examples_b = examples_t + examples_h;

        if ( pageYOffset >= 550 && pageYOffset < examples_t) {
          creat.addClass('active');
        } else {
          creat.removeClass('active');
        }

      });

    }
  },

  copy: {

    init: function () {

      app.copy.change_color();
      app.copy.clipboard();

    },

    change_color: function () {

      var select = $('.copy__select');
      var checkbox = select.find('input[type="radio"]');
      var btn = $('.copy__btn');
      var btn_white = $('.copy__btn.copy__white');
      var btn_black = $('.copy__btn.copy__black');

      checkbox.on('change', function () {

        var $this = $(this);
        var label = $this.closest('label');

        if ( label.hasClass('white') ) {

          btn.removeClass('active show');
          btn_white.addClass('active');
          setTimeout(function () {
            btn_white.addClass('show');
          }, 10);

        } else if ( label.hasClass('black') ) {

          btn.removeClass('active show');
          btn_black.addClass('active');
          setTimeout(function () {
            btn_black.addClass('show');
          }, 10);

        }

      });

    },

    clipboard: function () {

      var btn = $('.btn-clipboard');

      btn.on('click', function (e) {
        e.preventDefault();

        var copy_btn = $('.copy__btn');

        copy_btn.each(function () {

          if ( $(this).hasClass('active') ) {

            var html = $(this).html();

            copyToClipboard(html);

            btn.text('Код скопирован');
            btn.addClass('active');

            setTimeout(function () {
              btn.text('Скопировать код');
              btn.removeClass('active');
            }, 3000);

          }
        })




      });

      const copyToClipboard = str => {
        const el = document.createElement('textarea');  // Create a <textarea> element
        el.value = str;                                 // Set its value to the string that you want copied
        el.setAttribute('readonly', '');                // Make it readonly to be tamper-proof
        el.style.position = 'absolute';
        el.style.left = '-9999px';                      // Move outside the screen to make it invisible
        document.body.appendChild(el);                  // Append the <textarea> element to the HTML document
        const selected =
            document.getSelection().rangeCount > 0        // Check if there is any content selected previously
                ? document.getSelection().getRangeAt(0)     // Store selection if found
                : false;                                    // Mark as false to know no selection existed before
        el.select();                                    // Select the <textarea> content
        document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)
        document.body.removeChild(el);                  // Remove the <textarea> element
        if (selected) {                                 // If a selection existed before copying
          document.getSelection().removeAllRanges();    // Unselect everything on the HTML document
          document.getSelection().addRange(selected);   // Restore the original selection
        }
      };

    }

  },

  mfp: {

    init: function () {

      $('.open-modal').magnificPopup({
        removalDelay: 300,
        mainClass: 'mfp-fade'
      });

    }

  }

};

$(document).ready(app.init());


























